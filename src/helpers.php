<?php

use SangBoy\GeoIP\GeoIP;

if(!function_exists('geoip')) {
    /**
     * 获取GeoIP实例
     * @param string $ip
     * @param string|null $driver
     * @return GeoIP
     */
    function geoip(string $ip, string $driver = null): GeoIP
    {
        return app('geoip', compact('ip', 'driver'));
    }
}
