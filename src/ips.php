<?php

return [
    [
        'start' => '',
        'end' => '',
        'country' => '',
        'province' => '',
        'province_code' => '',
        'city' => '',
        'city_code' => '',
        'area' => '',
        'area_code' => '',
        'isp' => '',
    ],
];
