<?php

namespace SangBoy\GeoIP;

use SangBoy\GeoIP\Exceptions\ConfigNotFoundException;
use SangBoy\GeoIP\Exceptions\MissingParamException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

/**
 * @property string|null country
 * @property string|null country_code
 * @property string|null province
 * @property string|null province_code
 * @property string|null city
 * @property string|null city_code
 * @property string|null area
 * @property string|null area_code
 * @property string|null isp
 * @property array raw
 */
class GeoIP
{
    /**
     * @var string
     */
    protected string $ip;

    /**
     * @var string
     */
    protected string $driver;

    /**
     * 已检索过的驱动
     * @var array
     */
    protected array $retrieved_drives = [];

    /**
     * @var Collection
     */
    protected Collection $drives;

    /**
     * @var array
     */
    protected array $geo_info = [];

    /**
     * @param string $ip
     * @param string|null $driver
     * @throws ConfigNotFoundException
     * @throws MissingParamException
     */
    public function __construct(string $ip, string $driver = null)
    {
        $this->ip = $ip;
        $this->driver = $driver ?: '';

        $this->loadConfig();

        $this->retrieve();
    }

    /**
     * 加载配置
     * @return void
     */
    protected function loadConfig()
    {
        $this->drives = collect(config('geoip.drives', []))->filter(fn($driver) => ($driver['enable'] ?? false));

        !$this->driver && ($this->driver = config('geoip.default_driver', $this->drives->keys()->first()));
    }

    /**
     * @return void
     * @throws ConfigNotFoundException
     * @throws MissingParamException
     */
    protected function check()
    {
        if (!$this->ip) {
            throw new MissingParamException('IP cannot be empty!');
        }
        if (!preg_match('/^((?:(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\.){3}(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d))))$/', $this->ip)) {
            throw new MissingParamException('Incorrect IP format!');
        }
        if ($this->drives->isEmpty()) {
            throw new ConfigNotFoundException('No drive available.Please add configuration in [config.geoip] file!');
        }
        if (!$this->drives->has($this->driver)) {
            throw new ConfigNotFoundException('Drive [' . $this->driver . '] does not exist!');
        }
        if (!$this->drives->get($this->driver)['enable']) {
            throw new ConfigNotFoundException('Drive [' . $this->driver . '] is off!');
        }
    }

    /**
     * 检索
     * @return bool
     * @throws ConfigNotFoundException
     * @throws MissingParamException
     */
    protected function retrieve(): bool
    {
        $this->check();

        if ($this->driver == 'local') {
            return $this->retrieveLocal();
        }
        $driver = $this->drives->get($this->driver);
        $host   = str_replace('{ip}', $this->ip, $driver['host']);

        try{
            $client   = new Client();
            $response = $client->get($host)->getBody()->getContents();
            $encode   = mb_detect_encoding($response, ['ASCII', 'UTF-8', 'GB2312', 'GBK', 'BIG5']);
            $response = mb_convert_encoding($response, 'UTF-8', $encode);
            $res      = json_decode($response, true);
            $data    = $driver['data_field'] ? Arr::get($res, $driver['data_field']) : $res;
            if ($data) {
                foreach ($driver['fields'] as $field => $org_field) {
                    $this->geo_info[$field] = $org_field ? Arr::get($data, $org_field) : null;
                }
                $this->geo_info['raw'] = $data;
                return true;
            }
        }catch (GuzzleException|RequestException $e){$e->getMessage();}

        if ($driver['tries'] == 0) {
            $this->drives = $this->drives->forget($this->driver);
            if (!config('geoip.polling_model', true) || $this->drives->isEmpty()) {
                return false;
            }
            $this->driver = $this->drives->keys()->first();
        } else {
            if (!config('geoip.polling_model', true)) {
                return false;
            }
            $driver['tries'] -= 1;
            $this->drives->put($this->driver, $driver);
            $driver['backoff'] && sleep($driver['backoff']);
        }

        return $this->retrieve();
    }

    /**
     * 在本地IP库中检索
     * @return bool
     */
    protected function retrieveLocal(): bool
    {
        //TODO
        return true;
    }

    /**
     * @param string $ip
     * @param string|null $driver
     * @return static
     * @throws ConfigNotFoundException
     * @throws MissingParamException
     */
    public static function make(string $ip, string $driver = null): GeoIP
    {
        return new static($ip, $driver);
    }

    /**
     * 获取GEO信息
     * @return array
     */
    public function geoInfo(): array
    {
        return $this->geo_info;
    }

    /**
     * @param string $name
     * @return string|integer|null
     */
    public function __get(string $name)
    {
        return $this->geo_info[$name] ?? null;
    }
}
