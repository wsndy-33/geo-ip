<?php

namespace SangBoy\GeoIP;

use Illuminate\Support\ServiceProvider;

class GeoIpServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('geoip', GeoIP::class);

        $this->registerPublishing();
        if(!config('geoip')) {
            config(['geoip' => require(__DIR__.'/../config/geoip.php')]);
        }
    }

    /**
     * 资源发布注册.
     *
     * @return void
     */
    protected function registerPublishing()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([__DIR__.'/../config' => config_path()], 'geoip-config');
        }
    }
}
