<?php

return [
    /**
     * 默认驱动器
     */
    'default_driver' => 'pconline',

    /**
     * 检索模式，默认轮询
     * polling（轮询），会循环调用所有已开启驱动器，直到找到为止，或驱动器全部遍历结束
     * single_drive（单驱动器），只在单个驱动器中检索，运行完尝试次数后停止继续检索
     */
    'polling_model' => false,

    /**
     * IP库驱动器
     */
    'drives' => [
        /**
         * 本地驱动
         */
        'local' => [
            'enable' => false,
            'cache'  => true
        ],

        /**
         * 太平洋网站
         */
        'pconline' => [
            /**
             * 开启状态
             */
            'enable' => true,

            /**
             * 最大尝试次数
             */
            'tries' => 1,

            /**
             *  重试任务前等待的秒数。
             */
            'backoff' => 0,

            'host' => 'http://whois.pconline.com.cn/ipJson.jsp?ip={ip}&json=true',

            /**
             * 其他携带参数
             */
            'params' => [],

            /**
             * 包含数据的键，置空时为最顶级
             */
            'data_field' => '',

            /**
             * geo信息对应字段
             */
            'fields' => [
                'country' => '',
                'country_code' => '',
                'province' => 'pro',
                'province_code' => 'proCode',
                'city' => 'city',
                'city_code' => 'cityCode',
                'area' => 'region',
                'area_code' => 'regionCode',
                'isp' => 'regionCode',
            ]

        ],

        /**
         * ip.useragentinfo.com
         */
        'useragentinfo' => [
            /**
             * 开启状态
             */
            'enable' => true,

            /**
             * 最大尝试次数
             */
            'tries' => 1,

            /**
             *  重试任务前等待的秒数。
             */
            'backoff' => 0,

            'host' => 'http://ip.useragentinfo.com/json?ip={ip}',

            /**
             * 其他携带参数
             */
            'params' => [],

            /**
             * 包含数据的键，置空时为最顶级
             */
            'data_field' => '',

            /**
             * geo信息对应字段
             */
            'fields' => [
                'country' => 'country',
                'country_code' => 'short_name',
                'province' => 'province',
                'province_code' => '',
                'city' => 'city',
                'city_code' => '',
                'area' => 'area',
                'area_code' => '',
                'isp' => 'isp',
            ]

        ],

        /**
         * 淘宝 TODO
         */
        'taobao' => [
            /**
             * 开启状态
             */
            'enable' => true,

            /**
             * 最大尝试次数
             */
            'tries' => 1,

            /**
             *  重试任务前等待的秒数。
             */
            'backoff' => 0,

            'host' => 'https://ip.taobao.com/outGetIpInfo?ip={ip}&accessKey=alibaba-inc',

            /**
             * 其他携带参数
             */
            'params' => [],

            /**
             * 包含数据的键，置空时为最顶级
             */
            'data_field' => 'data',

            /**
             * geo信息对应字段
             */
            'fields' => [
                'country' => 'country',
                'country_code' => 'country_id',
                'province' => 'region',
                'province_code' => 'region_id',
                'city' => 'city',
                'city_code' => 'city_id',
                'area' => 'area',
                'area_code' => 'area_id',
                'isp' => 'isp',
            ]

        ],

        /**
         * 百度
         */
        'baidu' => [
            /**
             * 开启状态
             */
            'enable' => true,

            /**
             * 最大尝试次数
             */
            'tries' => 1,

            /**
             *  重试任务前等待的秒数。
             */
            'backoff' => 0,

            'host' => 'http://opendata.baidu.com/api.php?query={ip}&co=&resource_id=6006&oe=utf8',

            /**
             * 其他携带参数
             */
            'params' => [],

            /**
             * 包含数据的键，置空时为最顶级
             */
            'data_field' => 'data.0',

            /**
             * geo信息对应字段
             */
            'fields' => [
                'country' => '',
                'country_code' => '',
                'province' => 'regionName',
                'province_code' => '',
                'city' => 'city',
                'city_code' => '',
                'area' => '',
                'area_code' => '',
                'isp' => 'location',
            ]

        ],

        /**
         * http://ip-api.com TODO
         */
        'ip-api' => [
            /**
             * 开启状态
             */
            'enable' => true,

            /**
             * 最大尝试次数
             */
            'tries' => 3,

            /**
             *  重试任务前等待的秒数。
             */
            'backoff' => 1,

            'host' => 'https://ip-api.com/json/{ip}?lang=zh-CN',

            /**
             * 其他携带参数
             */
            'params' => [],

            /**
             * 包含数据的键，置空时为最顶级
             */
            'data_field' => '',

            /**
             * geo信息对应字段
             */
            'fields' => [
                'country' => 'country',
                'country_code' => 'countryCode',
                'province' => 'regionName',
                'province_code' => '',
                'city' => 'city',
                'city_code' => '',
                'area' => '',
                'area_code' => '',
                'isp' => 'isp',
            ]

        ]
    ]
];
