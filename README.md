# sang-boy/geo-ip

#### 介绍
使用IP地址获取GEO信息。


#### 安装教程

1.  `composer require sang-boy/geo-ip`
2.  `php artisan vendor:publish --provider=SangBoy\GeoIP\GeoIpServiceProvider`

#### 使用说明

1.  使用GeoIP实例化的方式

```
use \SangBoy\GeoIP\GeoIP;
$geoip = new GeoIP('127.0.0.1');
$geoip->geoInfo();
```

2.  使用`geoip`帮助函数

```
geoip('127.0.0.1')->geoInfo()
```

返回信息为空数组或以下结构

```
[
    'country' => '国家',
    'country_code' => '国家代码',
    'province' => '省份',
    'province_code' => '省份代码',
    'city' => '城市',
    'city_code' => '城市代码',
    'area' => '地区',
    'area_code' => '地区代码',
    'isp' => '网络服务商',
    'raw' => [],//接口原数据
]
//以上数据各个参数并不一定都能拿到，会根据不同的接口有所变化，具体信息请自行配置驱动器测试
```


#### 感谢以下站点的贡献
1.  whois.pconline.com.cn
2.  ip.useragentinfo.com
3.  ip.taobao.com
4.  opendata.baidu.com
5.  ip-api.com

如有侵权，请邮件至koko-9527@qq.com。

